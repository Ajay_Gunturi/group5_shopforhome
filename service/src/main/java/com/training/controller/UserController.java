package com.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.model.ProductEntity;

import com.training.service.ProductService;




@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
	@Autowired
	ProductService prodService;
	
	@GetMapping("products")
	public List<ProductEntity> getProducts() {
		
		return prodService.getProducts();
	}
	@GetMapping("productByname")
	public ProductEntity getProductsByName(@RequestParam String name) {
		
		return prodService.getProductByName(name);
	}
	
	@GetMapping("productsByCategory")
	public List<ProductEntity> getProductsByCategory(@RequestParam String category) {
		
		return prodService.getProductsByCategory(category);
	}
	
}
