import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from '../service/rest.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
public productList:any;
retData: any;
  constructor(private rest:RestService,private router:Router,private http:HttpClient,) { }

  ngOnInit(): void {
    this.rest.getProduct()
    .subscribe(res=>{
      this.productList = res;
      this.productList.forEach((a:any) =>{
        Object.assign(a,{quantity:1,total:a.price});
      })
    })
  }

  editProduct(event: any){
    this.router.navigate(['/edit/product', event.target.id as number]);
  }

  getAllProducts(){
    const headers = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
      this.http.get<any>('http://localhost:8080/products', {headers:this.rest.authHeaders()}).subscribe(data => {
        this.retData = data;
  })
}

   deleteProduct(event: any){
    if(confirm('Are you sure you want to delete this product?')){
      const headers = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
        this.http.delete<any>('http://localhost:8080/delete/product?productId='.concat(event.target.id), {headers}).subscribe(data => {
          this.retData = this.getAllProducts();
      })
    }
  }

}
