INSERT INTO CAPSTONE_PROJECT.PRODUCTS (CATEGORY,DESCRIPTION,IMAGE,NAME,PRICE,STOCK,RATING) VALUES
	 ('Electronics','Apple iPhone','iphone13.jpg','iPhone 13',80000.0,20000,4),
	 ('Footwear','Sports Shoes','nike.jpg','Nike Shoes',2500.0,200,5),
	 ('Grocery','Detergent','excel.jpg','Surf Excel',100.0,500,3),
	 ('Beauty','Facewash','facewash.jpg','Himalaya Face Wash',70.0,100,4),
	 ('Electronics','Wireless Headphone','boat.jpg','Boat Bassheads',2000.0,7000,5),
	 ('Electronics','15 inch 11th Intel Core i7 Processor','dell.jpg','Dell Inspiron 15',60000.0,2000,5);
